# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.6](https://gitlab.com/miniest/mini-ui/compare/v0.0.5...v0.0.6) (2020-12-08)


### Features

* fonts are loaded locally instead of fetched online ([6a03677](https://gitlab.com/miniest/mini-ui/commit/6a0367710398c040b55e72dcbe15636b494875a2))

### [0.0.5](https://gitlab.com/miniest/mini-ui/compare/v0.0.4...v0.0.5) (2020-10-05)


### Bug Fixes

* prevent default behavior of toggle to navigate as link ([ad76399](https://gitlab.com/miniest/mini-ui/commit/ad763998ba55ac8adc5a6053f726e7ed4fb850b6))

### [0.0.4](https://gitlab.com/miniest/mini-ui/compare/v0.0.3...v0.0.4) (2020-07-09)


### Features

* mini-select-box component ([9dc4ae3](https://gitlab.com/miniest/mini-ui/commit/9dc4ae3f041503ddac5201d3e91e294c4af831c0))


### Bug Fixes

* calendar open in the current month, fixed value setting ([d4a7c44](https://gitlab.com/miniest/mini-ui/commit/d4a7c44b3ff97e9697df479df8f6709ceea3f7ca))

### [0.0.3](https://gitlab.com/miniest/mini-ui/compare/v0.0.2...v0.0.3) (2020-06-22)


### Features

* select-box ([035c163](https://gitlab.com/miniest/mini-ui/commit/035c1639f3886dbcfe683e78acb4fdeb3c5f0b4b))

### [0.0.2](https://gitlab.com/miniest/mini-ui/compare/v0.0.1...v0.0.2) (2020-05-25)


### Features

* initial set of features ([f149f7e](https://gitlab.com/miniest/mini-ui/commit/f149f7ebed80ffee7eb6e58240583fe4b20ddfa5))

### 0.0.1 (2020-05-14)
