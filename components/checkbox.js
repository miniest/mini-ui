function template() {
	return `
<style>
:host {
	width: 2em;
	height: 1em;
	border-radius: 100em;
	display: block !important;
	position: relative;
	transition: background 0.25s;
	box-shadow: inset 0 0 3px 1px #2093ac;
	background-color: white;
}

:host(.checked) {
	box-shadow: inset 0 0 3px 1px white;
	background-color: #2093ac;
}

:host(.checked) > #dot {
	background: radial-gradient(ellipse at 35% 35%, white 0%, white 100%);
	right: 0;
  box-shadow: inset 0 0 3px 1px #516c85;
	
}

:host(:not(.checked)) > #dot {
	background: radial-gradient(ellipse at 35% 35%, hsla(0, 0%, 100%, 1) 0%, #2093ac 100%);
	right: 50%;
}

#dot {
	position: absolute;
	border-radius: 5em;
	height: 100%;
	width: 50%;
	box-sizing: border-box;
	border: 2px #ffffff00 solid;
  background-clip: content-box !important;
	transition: background,right 0.25s;
	right: 50%;
}

</style>
<div id="dot"></div>
`
}
export class Checkbox extends HTMLElement {
	/** shadow root */

	#sr = null;

	static observedAttributes = ["check-selector"];
	constructor() {
		super();

		this.#sr = this.attachShadow({ mode: "open" });
		this.#sr.innerHTML = template();
	}

	attributeChangedCallback(name, ov, nv) {
	}

	get checked() {
			return this.classList.contains("checked");
	}

	set checked(val) {
			if (val && (!this.checked)) {
					this.classList.add("checked");
					this.fireEvent("change", {checked: true});


					if (this.hasAttribute("check-selector")) {
						document.querySelectorAll(this.getAttribute("check-selector")).forEach(v => {
							v.classList.add("checked");
						})
					}
			} else if (!(val) && this.checked) {
					this.classList.remove("checked");
					this.fireEvent("change", {checked: false})

					if (this.hasAttribute("check-selector")) {
						document.querySelectorAll(this.getAttribute("check-selector")).forEach(v => {
							v.classList.remove("checked");
						})
					}
			}
	}

	connectedCallback() {
			this.addEventListener("click", () => {
					this.checked = !this.checked;
			});
	}

	fireEvent(name, data) {
			this.dispatchEvent(new CustomEvent(name, {detail: data}));
	}
}

customElements.define("mini-checkbox", Checkbox);
