const template = `
<style>
:host {
	--green: #7ee67e;
	--red: #fbac67;
	--orange: #67c4fb;
	--gray: gray;
	--silver: silver;
}
#content {
	position: relative;
	display: flex;
	background-color: inherit;
	align-content: center;
	align-items: center;
	justify-content: center;
	justify-items: center;
	padding: 0.1em;
	padding-right: 0.6em;
}

.strip-placeholder {
	flex:1;
	height: 1.2em;
	min-width: 3em;
	margin: 2px;
	position: relative;
	filter: drop-shadow(0px 0px 2px black);
}
.strip-content {
	font-size: 100%;
	display: flex;
	justify-content: center;
	align-content: center;
	font-weight: bold;
	/* color: #000000; */
	/* text-shadow: 0px 0px 13px white; */
	background-clip: text;
	color: transparent;
	/* background: red; */
	-webkit-background-clip: text;
	background-image: linear-gradient(90deg, #ffffff 0%, #ffffff 50%, #000000 40%);
	/* mix-blend-mode: difference; */
	filter: drop-shadow(0px 0px 2px silver);
	font-size: 80%;
	align-content: center;
	align-items: center;
	justify-content: center;
	justify-items: center;
	flex: 1;
}

.strip {
	position: absolute;
	display: flex;
	left: 0;
	top: 0;
	bottom: 0;
	right: -0.5em;
	background-color: white;
	clip-path: polygon(0 0, calc(100% - 0.5em) 0, 100% 50%, calc(100% - 0.5em) 100%, 0 100%, 0.5em 50%);
	background-image: linear-gradient(90deg, #008000 0%, #008000 50%, #ffffff 40%);
	align-content: center;
	align-items: center;
	justify-content: center;
	justify-items: center;
}

:slotted(*) {
}
</style>
<div id="content"></div>
`;

class MultiProgress extends HTMLElement {

	static observedAttributes = ["slots"];

	#slots = 0;
	#shadow = null;
	#content = null;

	constructor() {
		super();

		this.#shadow = this.attachShadow({mode: "closed"});
		this.#shadow.innerHTML = template;

		this.#content = this.#shadow.querySelector("#content");

	}

	attributeChangedCallback(name, ov, nv) {
		switch (name) {
			case "slots" : this._changeSlotsCount(nv); break;
		}
	}

	_changeSlotsCount(slots) {
		this.#slots = slots;
		this.#content.querySelector("*")?.forEach(v => v.remove());

		for (let i = 0; i < this.#slots; ++i) {
			this.#content.innerHTML += `<div class="strip-placeholder"><div class="strip"><slot class="strip-content" name=\"slot-${i}\"></slot></div></div>`
		}

		this._redraw();
	}

	_redraw() {
		for (let i = 0; i < this.#slots; ++i) {
			let percent = parseInt(this.querySelector(`[slot=slot-${i}]`)?.getAttribute("data-percent") ?? 0);

			let color = "green";

			if (percent < 25) {
				color = "red";
			} else if (percent < 75) {
				color = "orange";
			} else if (Number.isNaN(percent)) {
				color = "gray";
			}

			let strip = this.#content.querySelectorAll(".strip-placeholder .strip")[i];

			if (Number.isNaN(percent)) {
				strip.style.backgroundImage = `repeating-linear-gradient(-45deg, var(--gray) 0, var(--gray) 5px, var(--silver) 5px, var(--silver) 10px)`;
				strip.querySelector(".strip-content").style.backgroundImage = `none`;
				strip.querySelector(".strip-content").style.backgroundColor = `white`;

			} else {
				strip.style.backgroundImage = `linear-gradient(90deg, var(--${color}) 0%, var(--${color}) ${percent}%, #ffffff ${percent}%)`;
				strip.querySelector(".strip-content").style.backgroundImage = `linear-gradient(90deg, white 0%, white ${percent}%, #607d8b ${percent}%)`;
			}

		}
	}
}


customElements.define("multi-progress", MultiProgress);
