export class SelectOption extends HTMLElement {
	#sr = null;

	static formAssociated = true;

	constructor() {
		super();

		this.#sr = this.attachShadow({mode: "open"});
		this.#sr.innerHTML = getTemplate();
	}
}

customElements.define("mini-option", SelectOption);

