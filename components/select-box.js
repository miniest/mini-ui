function getTemplate() {
	return `
	<style>
		@import url("/lib/@fortawesome/fontawesome-free/css/all.css");
		:host {
			position: relative;
			display: inline-flex;
			font: inherit;
			background-color: white;
			min-width: 10em;
			padding: 0.25em;
		}

		.content-line {
			flex: 1;
			display: flex;
			align-items: center;
		}

		.selected-content {
			flex: 1 100%;
			position: relative;
			width: -webkit-fill-available;
		}

		.content-line i {
			flex: 0;
		}

		slot[name='options']::slotted(*) {
			padding: 0.25rem;
			display: flex;
		}

		slot[name='options']::slotted(*:hover) {
			background-color: var(--mini-color-shade-light);
			cursor: pointer;
		}

		.popup {
			background-color: white;
			position: absolute;
			top: 100%;
			right: 0;
			min-width: 100%;
			display: flex;
			flex-direction: column;
			height: fit-content;
			max-height: 200px;
			max-width: 100%;
			overflow-y: auto;

			padding: var(--mini-form-input-pad);
			outline: none;
			border: 1px var(--mini-color-shade-light) solid;
			font-family: var(--mini-font-family);
			font-size: 1rem;
			font: inherit;
			color: var(--mini-color-primary);
			box-sizing: border-box;
		}

		.popup {
			box-shadow: 0 0 0.25rem 0 var(--mini-color-shade-light);
		  //border-color: var(--primary-color);
	    //border: 1px var(--mini-color-shade-dark) solid;
			z-index: 2;
		}
		.popup.hidden {
			display: none;
		}
		._ban {
			margin-right: 0.25em;
			margin-left: 0.25em;
			color: #ffbebe;
		}

		slot[name='values'] {
			display: none;
		}
	</style>
	<div class="content-line"><div class="selected-content"></div><i class=" _ban fas fa-times"></i><i class="_chevron fas fa-chevron-down"></i></div>
	<div class="popup hidden"><slot name="options"></slot></div>
	<slot name="values"></slot>
`
}

export class SelectBox extends HTMLElement {
	#sr = null;
	#optionsSlot = null;
	#slottedClickHandler = null;
	#value = undefined;

	static formAssociated = true;

	constructor() {
		super();

		this.#sr = this.attachShadow({mode: "open"});
		this.#sr.innerHTML = getTemplate();

		this.#optionsSlot = this.#sr.querySelector("slot[name='options']");

		this.#optionsSlot.addEventListener("slotchange", this.slottedDataChanged.bind(this));

		this.#sr.querySelector(".content-line").addEventListener("click", this.onContentLineClick.bind(this))
		this.#sr.querySelector("._ban").addEventListener("click", this.onClearClick.bind(this))

		this.#slottedClickHandler = this.onSlottedClick.bind(this);
	}

	get value() {
		return this.#value;
	}

	slottedDataChanged(evt) {
		this.#optionsSlot.assignedElements().forEach(e => {
			e.removeEventListener("click", this.#slottedClickHandler);
			e.addEventListener("click", this.#slottedClickHandler);
		})
	}

	onContentLineClick(evt) {
		this.#sr.querySelector(".popup").classList.toggle("hidden");	
		this.classList.toggle("popped");	
	}

	onSlottedClick(evt) {
		const ref = this.#sr.querySelector(`slot[name='values']`).assignedElements().filter(e => e.getAttribute("data-value") === evt.target.getAttribute("data-value-ref")).pop();

		if (ref) {
			this.#sr.querySelector(".selected-content").innerHTML = "";
			let clone = ref.cloneNode(true);

			this.#sr.querySelector(".selected-content").innerHTML = `<slot name="selected"></slot>`;
			clone.setAttribute("slot", "selected");
			Array.from(this.querySelectorAll("[slot='selected']")).forEach(e => e.remove());
			this.appendChild(clone);

		} else {
			this.#sr.querySelector(".selected-content").innerHTML = evt.target.getAttribute("data-value");
		}
		this.#sr.querySelector(".popup").classList.add("hidden");	
		this.classList.remove("popped");	

		this.#value = evt.target.getAttribute("data-value");
	}

	onClearClick(evt) {
		evt.preventDefault();
		evt.stopPropagation();

		this.#sr.querySelector(".selected-content").innerHTML = "";
		this.#value = undefined;
		this.#sr.querySelector(".popup").classList.add("hidden");	
		this.classList.remove("popped");	

	}
}

customElements.define("mini-select-box", SelectBox);
