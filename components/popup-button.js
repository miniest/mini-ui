function getTemplate() {
	return `
<style>
:host {
	position: relative;
}

#header {
	position: relative;
}

:host([open]) > #content {
	display: block;
}

:host(:not([open])) > #content {
	display: none;
}

#content {
	position: absolute;
	z-index: 10;
	top: 100%;
	right: 0;
}

</style>

<div id="header"><slot name="header"></slot></div>
<slot id="content" name="content"></slot>
`
}
export class PopupButton extends HTMLElement {
	/** shadow root */
	#sr = null;
	#documentClickCloseHandler = this._closeOnDocumentClick.bind(this);
	constructor() {
		super();

		this.#sr = this.attachShadow({mode: "open"});

		this.#sr.innerHTML = getTemplate();
		this.#sr.querySelector("#header").addEventListener("click", this.toggleContentVisibility.bind(this))

		console.log(this.#sr)
	}

	toggleContentVisibility(evt) {
		evt.preventDefault();
		evt.stopPropagation();

		if (this.hasAttribute("open")) {
			this.removeAttribute("open");
			document.removeEventListener("click", this.#documentClickCloseHandler)
		} else {
			this.setAttribute("open", "");

			document.addEventListener("click", this.#documentClickCloseHandler);
		}
	}

	_closeOnDocumentClick(evt) {
			if (!evt.path.some(v => v === this)) {
				this.removeAttribute("open");
				document.removeEventListener("click", this.#documentClickCloseHandler)
			}
	}


}

customElements.define("mini-popup-button", PopupButton);
