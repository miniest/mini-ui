function getTemplate() {
	return `
	<style>
		:host > .open-slot {
			display: none;
		}
		:host > .close-slot {
			display: inline-block;
		}

		:host(.toggled) > .open-slot {
			display: inline-block;
		}
		:host(.toggled) > .close-slot {
			display: none;
		}
	</style>
	<div class="open-slot"> <slot name="open"></slot></div>
	<div class="close-slot"><slot name="close"></slot></div>
	`
}

export class MiniToggle extends HTMLElement {

		#sr = null;

		static observedAttributes = ["class"];

    constructor() {
        super();

				this.#sr = this.attachShadow({mode: "open"});
				this.#sr.innerHTML = getTemplate();
    }

    get toggled() {
        return this.classList.contains("toggled");
    }

    set toggled(val) {
			if (val) {
					this.classList.add("toggled");
					this.fireEvent("change", {toggled: true})

					if (this.hasAttribute("toggle-selector")) {
						document.querySelectorAll(this.getAttribute("toggle-selector")).forEach(v => {
							v.classList.add(this.getAttribute("toggle-class") ?? "toggled");
						})
					}
			} else  {
					this.classList.remove("toggled");
					this.fireEvent("change", {toggled: false})

				if (this.hasAttribute("toggle-selector")) {
					document.querySelectorAll(this.getAttribute("toggle-selector")).forEach(v => {
						v.classList.remove(this.getAttribute("toggle-class") ?? "toggled");
					})
				}
			}
    }

		attributeChangedCallback(name, ov, nv) {
			switch (name) {
				case "class": (ov !== nv) && (this.toggled = this.toggled = nv.split(" ").includes("toggled"));
			}
		}
    connectedCallback() {
        this.addEventListener("click", (evt) => {
            this.toggled = !this.toggled;

            evt.preventDefault();
        });

				if (this.classList.contains("toggled")) {
					this.toggled = true;
				} else {
					this.toggled = false;
				}
    }

    fireEvent(name, data) {
        this.dispatchEvent(new CustomEvent(name, {detail: data}));
    }

}

customElements.define('mini-toggle', MiniToggle);
