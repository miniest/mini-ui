import {Calendar} from "./calendar.js";

function getTemplate() {
	return `
<style>
	@import url("/lib/@fortawesome/fontawesome-free/css/all.min.css");
	:host {
		display: flex;
	}

	:host(:focus-within) #edit-wrapper {
		display: flex;
	}

	#edit-wrapper {
		display: none;
	}

	:host(:focus-within) #view-wrapper {
		display: none;
	}

	#view-wrapper {
		display: flex;
    flex: 1;
		justify-content: flex-end;
		height: calc(1em + 4px); /* padding and border of input */
	}

	#wrapper {
		display: flex;
		flex-direction: row;
		position: relative;
		justify-content: flex-end;
	}

	input {
		border: none;
		color: inherit;
		text-align: right;
		height: 1em;
		font: inherit;
		margin-top: 1px; /* nasty align to preven jumping in the calendar-input */
	}

	input:FOCUS {
		outline: none;
	}

	.icon-holder {
		display: none;
		margin-left: 0.5em;
	}

	[hidden] {
		display: none;
	}

	#year-input {
		width: 4ch;
	}

	#month-input {
		width: 2ch;
	}
	#day-input {
		width: 2ch;
	}

	#popup-calendar {
		position: absolute;
		top: 100%;
		right: -1em;
		background-color: white;
/* copied from panel */
		box-shadow: 0 0 3px 1px var(--mini-color-shade);
		padding: var(--mini-form-pad);
		margin: 0.5rem;
	}
</style>
<div id="wrapper">
	<div id="edit-wrapper"><input id="day-input"><span>.</span><input id="month-input"><span>.</span><input id="year-input"><mini-calendar exportparts="icon-button" hidden id="popup-calendar"></mini-calendar></div>
	<div id="view-wrapper"></div>
</div>
`
}

export class CalendarInput extends HTMLElement {

	/** shadow rootl */
	#sr = null;
	#internals = null;
	#value_year = null;
	#value_month = null;
	#value_day = null;
	#is_set = false;

	static formAssociated = true;

	constructor() {
		super();

		this.#sr = this.attachShadow({mode: "open"});
		this.#sr.innerHTML = getTemplate();
		this.#internals = this.attachInternals();

		let today = new Date();
		this.#value_year = `${today.getFullYear()}`;
		this.#value_month = `${(today.getMonth() + 1).toString().padStart(2, "0")}`;
		this.#value_day = `${today.getDate().toString().padStart(2,"0")}`;
		this.#is_set = false;

		this.addEventListener("focus", this.onFocus.bind(this));

		this.#sr.querySelector("#popup-calendar").addEventListener("change", this.onPopupInput.bind(this));
	}
	
	get value() {
		return this.hasAttribute("value") ? this.getAttribute("value") : null;
	}

	set value(v) {
		if (v) {
			this.setAttribute("value", v);
		} else {
			this.removeAttribute("value");
		}
	}

	attributeChangedCallback(name, ov, nv) {
		switch (name) {
			case "value": (ov !== nv) && (this.value = nv); break;
		}
	}

	onFocus(evt) {
		this.#sr.querySelector("#day-input").focus();
		this.#sr.querySelector("#popup-calendar").removeAttribute("hidden");
	}

	connectedCallback() {
		if (!this.hasAttribute("tabindex")) {
			this.setAttribute("tabindex", 0);
		}

		this.updateView();
		this.updateEdit();

	}

	togglePopup(evt) {
		let pc = this.#sr.querySelector("#popup-calendar");

		if (pc.hasAttribute("hidden")) {
			pc.removeAttribute("hidden");
		} else {
			pc.setAttribute("hidden");
		}
	}

	onPopupInput(evt) {
		let data = evt.detail;
		let m = data.match(/(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/);
		
		if (m.groups.year && m.groups.month && m.groups.day) {
			this.#value_year = m.groups.year;
			this.#value_month = m.groups.month;
			this.#value_day = m.groups.day

			this.#is_set = true;
			this.blur();
		} else {
			throw `invalid date: ${data}`
		}

		this.setValue();
	}


	_partsToString() {
		return `${this.#value_year}-${this.#value_month}-${this.#value_day}`;
	}

	setValue() {
		this.#internals.setFormValue(this._partsToString(), this._partsToString());

		this.updateView();
		this.updateEdit();
		this.value = this._partsToString()
	}

	updateView() {
		this.#sr.querySelector("#view-wrapper").innerHTML = this.#is_set ? `${this.#value_day}.${this.#value_month}.${this.#value_year}` : "";
	}

	updateEdit() {
		this.#sr.querySelector("#year-input").value = this.#is_set ? this.#value_year : "";
		this.#sr.querySelector("#month-input").value = this.#is_set ? this.#value_month : "";
		this.#sr.querySelector("#day-input").value = this.#is_set ? this.#value_day : "";

		this.#sr.querySelector("#popup-calendar").setAttribute("value", this.#is_set ? this._partsToString() : "")
	}
}

customElements.define("mini-calendar-input", CalendarInput);
