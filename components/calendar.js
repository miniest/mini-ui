function getTemplate() {
	return `
<style>
		@import url("/lib/@fortawesome/fontawesome-free/css/all.min.css");

		:host {
			--primary-color: black;

			display: flex;
			flex: 1;
			flex-direction: column;
			user-select: none;
		}

		#header {
			flex: 1;
			display: flex;
			flex-direction: row;
	    justify-content: space-between;
			padding: 0 1px;
		}

		#data {
			width: 100%;
			border-spacing: 1px;
			background-color: silver;
			font-size: 0.75em;
		}

		td {
			width: 1.75em;
			height: 1.75em;
			text-align: center;
			background-color: white;
			border: 1px transparent solid;
		}

		#data > tbody td:hover {
			background-color: #00BCD4;
			border: 1px white solid;
			filter: drop-shadow(0 0 3px gray);
			font-weight: bold;
		}

		#data > thead td {
			background-color: darkslategray;
			color: white;
			font-weight: bold;
		}

		#year-text, #month-text {
			padding: 0 0.5em;
			display: inline-block;
			width: 4ch;
			text-align: center;
		}

		.icon-button {
		}

		#year-selector, #month-selector {
			display: flex;
			flex-wrap: nowrap;
			align-items: center;
			margin-left: 0.25em;
		}

		.other-month {
			background-color: silver;
		}

		.value {
			background-color: green;
		}
</style>
<div id="header">
	<div id="year-selector"><a class="icon-button" part="icon-button" id="year-left"><i class="far fa-caret-square-left"></i></a><span id="year-text">2020</span><a class="icon-button" part="icon-button" id="year-right"><i class="far fa-caret-square-right"></i></a></div>
	<div id="month-selector"><a class="icon-button" part="icon-button" id="month-left"><i class="far fa-caret-square-left"></i></a><span id="month-text">2020</span><a class="icon-button" part="icon-button" id="month-right"><i class="far fa-caret-square-right"></i></a></div>
</div>
	<table id="data">
		<thead>
			<td>M</td>
			<td>T</td>
			<td>W</td>
			<td>T</td>
			<td>F</td>
			<td>S</td>
			<td>S</td>
		</thead>
		<tbody></tbody>
	</table>
`;
}

export class Calendar extends HTMLElement {
	#sr = null;
	#today = new Date();
	#month = this.#today.getMonth();
	#year = this.#today.getFullYear();
	
	static observedAttributes = ["value"];

	constructor() {
		super();

		this.#sr = this.attachShadow({mode: "open"});
		this.#sr.innerHTML = getTemplate();

		this.generateData(this.#year, this.#month);

		this.#sr.querySelector("#month-left").addEventListener("click", this.subtractMonth.bind(this))
		this.#sr.querySelector("#month-right").addEventListener("click", this.addMonth.bind(this))
		this.#sr.querySelector("#year-left").addEventListener("click", this.subtractYear.bind(this))
		this.#sr.querySelector("#year-right").addEventListener("click", this.addYear.bind(this))
		
		this.#sr.querySelector("#month-text").innerHTML = (this.#month + 1).toString().padStart(2, "0");
		this.#sr.querySelector("#year-text").innerHTML = this.#year.toString().padStart(4, "0");
	}

	get value() {
		return this.hasAttribute("value") ? this.getAttribute("value") : null;
	}

	set value(v) {
		if (v) {
			this.setAttribute("value", v);
		} else {
			this.removeAttribute("value");
		}
	}

	attributeChangedCallback(name, ov, nv) {
		switch (name) {
			case "value": (ov !== nv) && (this.value = nv); break;
		}
	}

	/**
	 *
	 * @param month = zero based month
	 */
	generateData(year, month) {
		let date = new Date(year, month);
		const body = this.#sr.querySelector("#data tbody");

		// monday is 1
		let curr_week_day = date.getDay();

		// fix sunday
		if (curr_week_day == 0) {
			curr_week_day = 7;
		}

		// align date to monday
		date.setDate( -( curr_week_day - 2 ));
		// move one week further if it is monday
		if (curr_week_day === 1) {
			date.setDate(date.getDate() - 7)
		} else if (curr_week_day === 0) {

		}

		// generate first 4 weaks
		let dates = [];
		while (true) {
			if (month === 0) {
				// special handling of first month
				if (date.getMonth() === 1) {
					// we are in the next month
					if (date.getDay() === 1) {
						// it is monday next month so stop
						break;
					}
				}

			} else if (month === 11) {
				// specia handling of last month
				if (date.getMonth() === 0) {
					// we are in the next month
					if (date.getDay() === 1) {
						// it is monday next month so stop
						break;
					}
				}

			} else {
				if (date.getMonth() > month) {
					// we are in the next month
					if (date.getDay() === 1) {
						// it is monday next month so stop
						break;
					}
				}
			}

			dates.push(new Date(date));
			date.setDate(date.getDate() + 1);
		}

		if (dates.length < ( 7 * 6)) {
			// generate last row if needed
			date = new Date(dates[dates.length - 1]);
			for (let i = 0; i < 7; ++i) {
				dates.push(new Date(date));
				date.setDate(date.getDate() + 1);
			}
		}

		let rows = "";
		for (let i = 0; i < 6; ++i) {
			rows += "<tr>";
			for (let j = 0; j < 7; ++j) {
				let cellDate = dates[i*7+j];
				let cellDateString = `${cellDate.getFullYear()}-${(cellDate.getMonth()+1).toString().padStart(2,"0")}-${(cellDate.getDate()).toString().padStart(2, "0")}`;
				
				rows += `<td class="${cellDate.getMonth() === month ? "this-month" : "other-month"}${this.value === cellDateString ? "value" : ""}" value="${cellDateString}">${cellDate.getDate()}</td>`
			}
			rows += "</tr>";
		}

		body.innerHTML = rows;

		// register click event to all cells
		body.querySelectorAll("td").forEach(v => v.addEventListener("click", this.onCellClick.bind(this)));
	}

	onCellClick(evt) {
		this.dispatchEvent(new CustomEvent("change", {detail: evt.target.getAttribute("value")}));
		this.value = evt.target.getAttribute("value");
	}

	subtractMonth(evt) {
		evt.preventDefault();

		let d = new Date(this.#year, this.#month);
		d.setMonth(d.getMonth() - 1);

		this.#year = d.getFullYear();
		this.#month = d.getMonth();

		this.generateData(this.#year, this.#month);

		this.#sr.querySelector("#month-text").innerHTML = (this.#month + 1).toString().padStart(2, "0");
		this.#sr.querySelector("#year-text").innerHTML = this.#year.toString().padStart(4, "0");
	}

	addMonth(evt) {
		evt.preventDefault();

		let d = new Date(this.#year, this.#month);
		d.setMonth(d.getMonth() + 1);

		this.#year = d.getFullYear();
		this.#month = d.getMonth();

		this.generateData(this.#year, this.#month);

		this.#sr.querySelector("#month-text").innerHTML = (this.#month + 1).toString().padStart(2, "0");
		this.#sr.querySelector("#year-text").innerHTML = this.#year.toString().padStart(4, "0");
	}

	subtractYear(evt) {
		evt.preventDefault();

		let d = new Date(this.#year, this.#month);
		d.setFullYear(d.getFullYear() - 1);

		this.#year = d.getFullYear();
		this.#month = d.getMonth();

		this.generateData(this.#year, this.#month);

		this.#sr.querySelector("#month-text").innerHTML = (this.#month + 1).toString().padStart(2, "0");
		this.#sr.querySelector("#year-text").innerHTML = (this.#year).toString().padStart(4, "0");
	}

	addYear(evt) {
		evt.preventDefault();

		let d = new Date(this.#year, this.#month);
		d.setFullYear(d.getFullYear() + 1);

		this.#year = d.getFullYear();
		this.#month = d.getMonth();

		this.generateData(this.#year, this.#month);

		this.#sr.querySelector("#month-text").innerHTML = (this.#month + 1).toString().padStart(2, "0");
		this.#sr.querySelector("#year-text").innerHTML = (this.#year).toString().padStart(4, "0");
	}
}


customElements.define("mini-calendar", Calendar);
