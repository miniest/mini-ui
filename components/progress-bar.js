const template = `
    <style>
	    :host {
             border-radius: 0.1rem;
            border: 1px solid var(--shade-color);
            padding: 1px;
            box-sizing: border-box;
            background-clip: content-box;
            display: flex;
			flex: auto;
			text-align: center;
		}

		:host.horizontal {
		}

        .strip {
            flex: 1;
			background-clip: content-box;
        }

        .strip.indicating {
            animation: pulse 3s 1s infinite;
        }

        ::slotted(*) {
        //mix-blend-mode: difference;
        }

        @keyframes pulse {
            from {filter: opacity(1);}
            80% {filter: opacity(1);}
            90% {filter: opacity(0.7);}
            to {filter: opacity(1);}
        }
    </style>
    <div class="strip"><slot name="text">text</slot></div>
`;

export class ProgressBar extends HTMLElement {
    constructor() {
        super();

        this._shadow = this.attachShadow({mode: "open"});
        this._shadow.innerHTML = template;

        this._strip = this._shadow.querySelector('.strip');
    }

    static get observedAttributes() { return ["percent"]; }

    attributeChangedCallback(name, ov, nv) {
        switch (name) {
            case "percent": {
                    let color = "black";
                    let val = parseInt(nv);
                    this._strip.classList.add("indicating");
                    switch (true) {
                        case (val < 25): {
                            color = "#ff7171";
                        }; break;
                        case (val < 75): {
                            color = "#ffc671";
                        }; break;
                        case (val === 100): {
                            color = "#CDDC39";
                            this._strip.classList.remove("indicating");
                        }; break;
                        default: {
                            color = "#ffff71";
                        }
                    }

                    let g = `linear-gradient(90deg, ${color} 0%, ${color} ${nv ?? 0}%, rgba(245,245,245,1) ${nv ?? 0}%)`;
                    this._strip.style.background = g;
            };
                break;
        }
    }
}

customElements.define('mini-progress-bar', ProgressBar);
