<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link href="../themes/default.css" rel="stylesheet">
		<link href="../css/reset.css" rel="stylesheet">
		<link href="../css/typography.css" rel="stylesheet">
		<link href="../css/flex.css" rel="stylesheet">
		<link href="../css/tag.css" rel="stylesheet">
		<script src="../components/calendar.js" type="module" async></script>
	</head>
	<body>
		<div id="main-container" class="flex col">
			<h1 class="align-hcenter">TAG</h1>
			<div class="flex row align-hcenter">
				<div class="align-hjustify">
					<mini-calendar></mini-calendar>
				</div>
			</div>
		</div>
	</body>
</html>
